FROM python:3-slim-buster

COPY requirements.txt /src/

RUN pip install -r /src/requirements.txt

RUN pip install jupyter nbval pytest-cov