# Gravitational Lensing

Here we analyse strong gravitational lenses, an astrophysical phenomenon where a galaxy
appears multiple times because its light is bent by the gravitational field of an intervening foreground lens galaxy.

Here is a schematic of a strong gravitational lens:

![Schematic of Gravitational Lensing](./assets/schematic.jpg)

Please follow the instructions in https://docs.docker.com/engine/install for the docker installation. Run the below command for interactive notebooks
```
docker-compose up
```

This notebook gives an overview.


```python
%matplotlib inline

import utils as al
import utils.plot as aplt

from astropy import cosmology as cosmo
import matplotlib.pyplot as plt
from os import path
```

    WARNING:confutil.conf:Pushing new config with path /workspace/utils/utils/arrayutil/config
    WARNING:confutil.conf:No configuration found at path /workspace/config
    2021-10-09 09:45:06,356 - confutil.conf - WARNING - Pushing new config with path /workspace/utils/utils/fitutil/config
    2021-10-09 09:45:06,412 - confutil.conf - WARNING - No configuration found at path /workspace/config


    2021-10-09 09:45:06,468 - confutil.conf - WARNING - Pushing new config with path /workspace/utils/utils/galaxyutil/config
    2021-10-09 09:45:06,533 - confutil.conf - WARNING - No configuration found at path /workspace/config
    2021-10-09 09:45:06,552 - confutil.conf - WARNING - Pushing new config with path /workspace/utils/config
    2021-10-09 09:45:06,624 - confutil.conf - WARNING - No configuration found at path /workspace/config


Lensing
-------
First, we illustrate lensing calculations here by creating an an image of a strong lens.

To describe the deflection of light, here we `Grid2D` data structures, which are two-dimensional
Cartesian grids of (y,x) coordinates.

Below, we make and plot a uniform Cartesian grid:


```python
grid = al.Grid2D.uniform(
    shape_native=(150, 150),
    pixel_scales=0.05,  # <- The pixel-scale describes the conversion from pixel units to arc-seconds.
)

grid_plotter = aplt.Grid2DPlotter(grid=grid)
grid_plotter.figure_2d()
```


    
![png](./assets/output_4_0.png)
    


Our aim is to create an image of the source galaxy after its light has been deflected by the mass of the foreground
lens galaxy. We therefore need to ray-trace the `Grid2D`'s coordinates from the 'image-plane' to the 'source-plane'.

We therefore need analytic functions representing a galaxy's light and mass distributions. For this, we use `Profile` objects, for example an ellipical sersic `LightProfile` object which represents a light distribution:


```python
sersic_light_profile = al.lp.EllSersic(
    centre=(0.0, 0.0),
    elliptical_comps=(0.2, 0.1),
    intensity=0.005,
    effective_radius=2.0,
    sersic_index=4.0,
)
```

By passing this profile the `Grid2D`, we can evaluate the light emitted at every (y,x) coordinate on the `Grid2D` and
create an image of the `LightProfile`.


```python
image = sersic_light_profile.image_2d_from_grid(grid=grid)

plt.imshow(image.native) # The use of 'native' is described at the start of the HowToLens tutorials.
```




    <matplotlib.image.AxesImage at 0x7f6fff083700>




    
![png](./assets/output_8_1.png)
    


The plot module provides methods for plotting objects and their properties, like the image of
a `LightProfile`.

Note how, unlike the matplotlib method above, this figure is displayed with axis units of arc-seconds, a colorbar,
labels, a title, etc. It takes care of all the heavy lifting that comes with making figures!


```python
light_profile_plotter = aplt.LightProfilePlotter(
    light_profile=sersic_light_profile, grid=grid
)
light_profile_plotter.figures_2d(image=True)
```


    
![png](./assets/output_10_0.png)
    


We use `MassProfile` objects to represent a galaxy's mass distribution and perform ray-tracing
calculations.

Below we create an elliptical isothermal `MassProfile` and compute its deflection angles on our Cartesian grid, where
the deflection angles describe how the lens galaxy's mass bends the source's light:


```python
isothermal_mass_profile = al.mp.EllIsothermal(
    centre=(0.0, 0.0), elliptical_comps=(0.1, 0.0), einstein_radius=1.6
)

deflections = isothermal_mass_profile.deflections_2d_from_grid(grid=grid)
```

The deflection angles are plotted.


```python
mass_profile_plotter = aplt.MassProfilePlotter(
    mass_profile=isothermal_mass_profile, grid=grid
)
mass_profile_plotter.figures_2d(
    deflections_y=True, deflections_x=True
)
```


    
![png](./assets/output_14_0.png)
    



    
![png](./assets/output_14_1.png)
    


Many other lensing quantities are easily plotted with the `MassProfilePltoter`.


```python
mass_profile_plotter.figures_2d(
    convergence=True,
    magnification=True,
)
```


    
![png](./assets/output_16_0.png)
    



    
![png](./assets/output_16_1.png)
    


Here a `Galaxy` object is a collection of `LightProfile` and `MassProfile` objects at a given redshift.

The code below creates two galaxies representing the lens and source galaxies shown in the strong lensing diagram above.


```python
lens_galaxy = al.Galaxy(
    redshift=0.5, light=sersic_light_profile, mass=isothermal_mass_profile
)

source_light_profile = al.lp.EllExponential(
    centre=(0.3, 0.2),
    elliptical_comps=(0.1, 0.0),
    intensity=0.1,
    effective_radius=0.5
)

source_galaxy = al.Galaxy(redshift=1.0, light=source_light_profile)
```

We can use a `GalaxyPlotter` to plot the properties of the lens and source galaxies.


```python
lens_galaxy_plotter = aplt.GalaxyPlotter(galaxy=lens_galaxy, grid=grid)
lens_galaxy_plotter.figures_2d(image=True, deflections_y=True, deflections_x=True)

source_galaxy_plotter = aplt.GalaxyPlotter(galaxy=source_galaxy, grid=grid)
source_galaxy_plotter.figures_2d(image=True)
```


    
![png](./assets/output_20_0.png)
    



    
![png](./assets/output_20_1.png)
    



    
![png](./assets/output_20_2.png)
    



    
![png](./assets/output_20_3.png)
    


The geometry of the strong lens system depends on the cosmological distances between the Earth, the lens galaxy and
the source galaxy. It therefore depends on the redshifts of the `Galaxy` objects.

By passing these `Galaxy` objects to a `Tracer`, we use these galaxy redshifts and a cosmological model
to create the appropriate strong lens system.


```python
tracer = al.Tracer.from_galaxies(
    galaxies=[lens_galaxy, source_galaxy], cosmology=cosmo.Planck15
)

image = tracer.image_2d_from_grid(grid=grid)

tracer_plotter = aplt.TracerPlotter(tracer=tracer, grid=grid)
tracer_plotter.figures_2d(image=True)
```


    
![png](./assets/output_22_0.png)
    


The `TracerPlotter` includes the `MassProfile` quantities we plotted previously, which can be plotted as a subplot
that plots all these quantities simultaneously.

All of the objects introduced above are extensible. `Galaxy` objects can take many `Profile`'s and `Tracer`'s
many `Galaxy`'s.

If the galaxies are at different redshifts a strong lensing system with **multiple lens planes** will be created,
performing complex **multi-plane** ray-tracing calculations.

To finish, lets create a `Tracer` with 3 galaxies at 3 different redshifts, forming a system with two distinct Einstein
rings! The mass distribution of the first galaxy has separate components for its stellar mass and dark matter, where
the stellar components use a `LightAndMassProfile` via the `lmp` module.


```python
lens_galaxy_0 = al.Galaxy(
    redshift=0.5,
    bulge=al.lmp.EllSersic(
        centre=(0.0, 0.0),
        elliptical_comps=(0.0, 0.05),
        intensity=0.5,
        effective_radius=0.3,
        sersic_index=3.5,
        mass_to_light_ratio=0.6,
    ),
    disk=al.lmp.EllExponential(
        centre=(0.0, 0.0),
        elliptical_comps=(0.0, 0.1),
        intensity=1.0,
        effective_radius=2.0,
        mass_to_light_ratio=0.2,
    ),
    dark=al.mp.SphNFW(centre=(0.0, 0.0), kappa_s=0.08, scale_radius=30.0),
)

lens_galaxy_1 = al.Galaxy(
    redshift=1.0,
    bulge=al.lp.EllExponential(
        centre=(0.00, 0.00),
        elliptical_comps=(0.05, 0.05),
        intensity=1.2,
        effective_radius=0.1,
    ),
    mass=al.mp.EllIsothermal(
        centre=(0.0, 0.0), elliptical_comps=(0.05, 0.05), einstein_radius=0.6
    ),
)

source_galaxy = al.Galaxy(
    redshift=2.0,
    bulge=al.lp.EllSersic(
        centre=(0.0, 0.0),
        elliptical_comps=(0.0, 0.111111),
        intensity=0.7,
        effective_radius=0.1,
        sersic_index=1.5,
    ),
)

tracer = al.Tracer.from_galaxies(galaxies=[lens_galaxy_0, lens_galaxy_1, source_galaxy])

tracer_plotter = aplt.TracerPlotter(tracer=tracer, grid=grid)
tracer_plotter.figures_2d(image=True)
```


    
![png](./assets/output_25_0.png)
    


Lens Modeling
-------------

Lens modeling is the process of taking data of a strong lens (e.g. imaging data from the Hubble Space Telescope or
interferometer data from ALMA) and fitting it with a lens model, to determine the `LightProfile`'s and `MassProfile`'s
that best represent the observed strong lens.


```python
from utils import fitutil as af
```

In this example, we consider Hubble Space Telescope imaging of the strong lens SLACS2303+1422. First, lets load this
imaging dataset and plot it.

Note that the luminous emission of the foreground lens galaxy has been pre-subtracted in this image, to better
highlight the source.


```python
dataset_path = path.join("dataset", "slacs", "slacs2303+1422")

imaging = al.Imaging.from_fits(
    image_path=path.join(dataset_path, "image.fits"),
    psf_path=path.join(dataset_path, "psf.fits"),
    noise_map_path=path.join(dataset_path, "noise_map.fits"),
    pixel_scales=0.05,
)

imaging_plotter = aplt.ImagingPlotter(imaging=imaging)
imaging_plotter.figures_2d(image=True)
```


    
![png](./assets/output_29_0.png)
    


We next mask the dataset, to remove the exterior regions of the image that do not contain emission from the lensed
source galaxy.

Note how when we plot the `Imaging` below, the figure now zooms into the masked region.


```python
mask = al.Mask2D.circular(
    shape_native=imaging.shape_native,
    pixel_scales=imaging.pixel_scales,
    radius=3.0
)

imaging = imaging.apply_mask(mask=mask)

imaging_plotter = aplt.ImagingPlotter(imaging=imaging)
imaging_plotter.figures_2d(image=True)

```


    
![png](./assets/output_31_0.png)
    


We now compose the lens model that we fit to the data using `Model` objects. This behave analogously to the `Galaxy`,
`LightProfile` and `MassProfile` objects above, however their parameters are not specified and are instead determined
by a fitting procedure.

We will fit our strong lens data with two galaxies:

- A lens galaxy with an `EllIsothermal` `MassProfile` representing its mass.
- A source galaxy with an `EllExponential` `LightProfile` representing a disk.

The redshifts of the lens (z=0.5) and source(z=1.0) are fixed.


```python
lens_galaxy_model = af.Model(
    al.Galaxy,
    redshift=0.5,
    mass=al.mp.EllIsothermal
)

source_galaxy_model = af.Model(al.Galaxy, redshift=1.0, disk=al.lp.EllExponential)
```

We combine the lens and source model galaxies above into a `Collection`, which is the model we will fit. Note how
we could easily extend this object to compose highly complex models containing many galaxies.


```python
model = af.Collection(lens=lens_galaxy_model, source=source_galaxy_model)
```

By printing the `Model`'s we see that each parameters has a prior associated with it, which is used by the
model-fitting procedure to fit the model.


```python
print(lens_galaxy_model)
print()
print(source_galaxy_model)

```

    Galaxy (centre_0, GaussianPrior, mean = 0.0, sigma = 0.1), (centre_1, GaussianPrior, mean = 0.0, sigma = 0.1), (elliptical_comps_0, GaussianPrior, mean = 0.0, sigma = 0.3), (elliptical_comps_1, GaussianPrior, mean = 0.0, sigma = 0.3), (einstein_radius, UniformPrior, lower_limit = 0.0, upper_limit = 3.0)
    
    Galaxy (centre_0, GaussianPrior, mean = 0.0, sigma = 0.3), (centre_1, GaussianPrior, mean = 0.0, sigma = 0.3), (elliptical_comps_0, GaussianPrior, mean = 0.0, sigma = 0.3), (elliptical_comps_1, GaussianPrior, mean = 0.0, sigma = 0.3), (intensity, LogUniformPrior, lower_limit = 1e-06, upper_limit = 1000000.0), (effective_radius, UniformPrior, lower_limit = 0.0, upper_limit = 30.0)


We now choose the 'non-linear search', which is the fitting method used to determine the set of `LightProfile`
and `MassProfile` parameters that best-fit our data.

In this example we use [dynesty](https://github.com/joshspeagle/dynesty), a nested sampling algorithm that in our
experience has proven very effective at lens modeling.


```python
search = af.DynestyStatic(name="overview_example")
```

    2021-10-09 09:45:12,870 - fitutil.non_linear.abstract_search - INFO - Creating search


To perform the model-fit, we create an `AnalysisImaging` object which contains the `log likelihood function` that the
non-linear search calls to fit the lens model to the data.


```python
analysis = al.AnalysisImaging(dataset=imaging)
```

To perform the model-fit we pass the model and analysis to the search's fit method. This will output results (e.g.,
dynesty samples, model parameters, visualization) to hard-disk.

Once a model-fit is running, code outputs the results of the search to hard-disk on-the-fly. This includes
lens model parameter estimates with errors non-linear samples and the visualization of the best-fit lens model inferred
by the search so far.


```python
# result = search.fit(model=model, analysis=analysis)
```

The animation below shows a slide-show of the lens modeling procedure. Many lens models are fitted to the data over
and over, gradually improving the quality of the fit to the data and looking more and more like the observed image.

![Lens Modeling Animation](./assets/lensmodel.gif "model")

The fit returns a `Result` object, which contains the best-fit `Tracer` and `FitImaging` as well as the full
posterior information of the non-linear search, including all parameter samples, log likelihood values and tools to
compute the errors on the lens model.

Simulating Lenses
-----------------

Here we provide tool for simulating strong lens data-sets, which can be used to test lens modeling pipelines
and train neural networks to recognise and analyse images of strong lenses.

Simulating strong lens images uses a `SimulatorImaging` object, which models the process that an instrument like the
Hubble Space Telescope goes through observe a strong lens. This includes accounting for the exposure time to
determine the signal-to-noise of the data, accounting for diffraction of the observed light by the telescope optics
and accounting for the background sky in the exposure which acts as a source of noise.


```python
psf = al.Kernel2D.from_gaussian(shape_native=(11, 11), sigma=0.1, pixel_scales=0.05)

simulator = al.SimulatorImaging(
    exposure_time=300.0, background_sky_level=1.0, psf=psf, add_poisson_noise=True
)
```

Once we have a simulator, we can use it to create an imaging dataset which consists of an image, noise-map and
Point Spread Function (PSF) by passing it a tracer and grid.

This uses the tracer above to create the image of the strong lens and then add the effects that occur during data
acquisition.


```python
imaging = simulator.from_tracer_and_grid(tracer=tracer, grid=grid)
```

By plotting a subplot of the `Imaging` dataset, we can see this object includes the observed image of the strong lens
(which has had noise and other instrumental effects added to it) as well as a noise-map and PSF:


```python
imaging_plotter = aplt.ImagingPlotter(imaging=imaging)
imaging_plotter.subplot_imaging()
```


    
![png](./assets/output_50_0.png)
    


Below, we show what a strong lens observation looks like for the lowest resolution instrument in these examples (the
Vera Rubin Observatory) and highest resolution instrument (Keck Adaptive Optics).

![vro](./assets/vro_image.png)

![ao](./assets/ao_image.png)

Pixelizations
-------------

Pixelizations reconstruct the source galaxy's light on a pixel-grid. Unlike `LightProfile`'s, they are able to
reconstruct the light of non-symmetric, irregular and clumpy sources.

The image below shows a pixelized source reconstruction of the strong lens SLACS1430+4105, where the source is
reconstructed on a Voronoi-mesh adapted to the source morphology, revealing it to be a grand-design face on spiral
galaxy:

![Pixelized Source](./assets/imageaxis.png)

Interferometry
--------------

Modeling of interferometer data from submillimeter and radio observatories.

Visibilities data is fitted directly in the uv-plane, circumventing issues that arise when fitting a dirty image
such as correlated noise. This uses the non-uniform fast fourier transform algorithm
[PyNUFFT](https://github.com/jyhmiinlin/pynufft) to efficiently map the lens model images to the uv-plane.

Given the irregular and clumpy nature of submm / radio sources, pixelized source reconstructions are vital for
modeling interferometer datasets. This would be a slow process,  however we use the linear algebra
library [PyLops](https://pylops.readthedocs.io/en/latest/) to ensure visibilities fitting is efficient, even for
datasets consisting of **tens of millions** of visibilities.

An overview of interferometer analysis is given in `interferometer/overview.ipynb` and
the `interferometer` package has example scripts for simulating datasets and lens modeling.

Point Sources
-------------

There are many lenses where the background source is not extended but is instead a point-source, for example strongly
lensed quasars and supernovae. For these objects, we do not want to model the source using a light profile, which
implicitly assumes an extended surface brightness distribution. Instead, we assume that our source is a point source
with a centre (y,x).

Here is an example of a compact source that has been simulated with the positions of its four
multiple images marked using stars:

![Point Source](./assets/image.png)

Groups & Clusters
-----------------

The strong lenses we've discussed so far have just a single lens galaxy responsible for the lensing. Group-scale
and cluster-scale strong lenses are systems where there tens or hundreds of lens galaxies deflecting the one or more
background sources:

![Group](./assets/groups_image.png)

![Cluster](./assets/cluster.png)

Modeling group-scale and cluster-scale lenses, with no limit on the number of
lens and source galaxies!


```python

```
