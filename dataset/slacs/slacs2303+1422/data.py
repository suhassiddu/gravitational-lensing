"""
Modeling: Mass Total + Source Parametric
========================================

In this script, we fit `Imaging` with a strong lens model where:

 - The lens galaxy's light is omitted (and is not present in the simulated data).
 - The lens galaxy's total mass distribution is an `EllIsothermal` and `ExternalShear`.
 - The source galaxy's light is a parametric `EllSersic`.
"""
# %matplotlib inline
# from pyprojroot import here
# workspace_path = str(here())
# %cd $workspace_path
# print(f"Working Directory has been set to `{workspace_path}`")

from os import path
import numpy as np
import utils.fitutil as af
import utils as al
import utils.plot as aplt

"""
__Dataset__

Load and plot the strong lens dataset `mass_sie__source_sersic` via .fits files, which we will fit with the lens model.
"""
dataset_name = "slacs2303+1422"
dataset_path = path.join("dataset", "slacs", dataset_name)

imaging = al.Imaging.from_fits(
    image_path=path.join(dataset_path, "image.fits"),
    psf_path=path.join(dataset_path, "psf.fits"),
    noise_map_path=path.join(dataset_path, "noise_map.fits"),
    pixel_scales=0.05,
)

imaging.data = imaging.image.resized_from(new_shape=(151, 151))
imaging.noise_map = imaging.noise_map.resized_from(new_shape=(151, 151))
imaging._psf = imaging.psf.resized_from(new_shape=(11, 11))

imaging_plotter = aplt.ImagingPlotter(imaging=imaging)
imaging_plotter.subplot_imaging()


np.save(file="image.npy", arr=imaging.image.native)
np.save(file="noise_map.npy", arr=imaging.noise_map.native)
np.save(file="psf.npy", arr=imaging.psf.native)
