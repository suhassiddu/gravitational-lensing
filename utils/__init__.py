import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "utils"))

import arrayutil
import confutil
import fitutil
import galaxyutil

from arrayutil import preprocess
from arrayutil.dataset.imaging import Imaging, SettingsImaging
from arrayutil.dataset.interferometer import Interferometer, SettingsInterferometer
from arrayutil.mask.mask_1d import Mask1D
from arrayutil.mask.mask_2d import Mask2D
from arrayutil.operators.convolver import Convolver
from arrayutil.inversion import pixelizations as pix, regularization as reg
from arrayutil.inversion.pixelizations import SettingsPixelization
from arrayutil.inversion.inversion.settings import SettingsInversion
from arrayutil.inversion.inversion.imaging import (
    inversion_imaging_from as InversionImaging,
)
from arrayutil.inversion.inversion.interferometer import (
    inversion_interferometer_from as InversionInterferometer,
)
from arrayutil.inversion.mappers import mapper as Mapper
from arrayutil.operators.transformer import TransformerDFT
from arrayutil.operators.transformer import TransformerNUFFT
from arrayutil.structures.arrays.one_d.array_1d import Array1D
from arrayutil.structures.arrays.two_d.array_2d import Array2D
from arrayutil.structures.arrays.values import ValuesIrregular
from arrayutil.structures.grids.one_d.grid_1d import Grid1D
from arrayutil.structures.grids.two_d.grid_2d import Grid2D
from arrayutil.structures.grids.two_d.grid_2d import Grid2DSparse
from arrayutil.structures.grids.two_d.grid_2d_interpolate import Grid2DInterpolate
from arrayutil.structures.grids.two_d.grid_2d_iterate import Grid2DIterate
from arrayutil.structures.grids.two_d.grid_2d_irregular import Grid2DIrregular
from arrayutil.structures.grids.two_d.grid_2d_irregular import Grid2DIrregularUniform
from arrayutil.structures.grids.two_d.grid_2d_pixelization import Grid2DRectangular
from arrayutil.structures.grids.two_d.grid_2d_pixelization import Grid2DVoronoi
from arrayutil.structures.vector_fields.vector_field_irregular import (
    VectorField2DIrregular,
)
from arrayutil.structures.kernel_2d import Kernel2D
from arrayutil.structures.visibilities import Visibilities
from arrayutil.structures.visibilities import VisibilitiesNoiseMap
from galaxyutil import util
from galaxyutil.galaxy.fit_galaxy import FitGalaxy
from galaxyutil.galaxy.galaxy import Galaxy, HyperGalaxy, Redshift
from galaxyutil.galaxy.galaxy_data import GalaxyData
from galaxyutil.hyper import hyper_data
from galaxyutil.plane.plane import Plane
from galaxyutil.profiles import (
    point_sources as ps,
    light_profiles as lp,
    mass_profiles as mp,
    light_and_mass_profiles as lmp,
    scaling_relations as sr,
)
from galaxyutil import convert

from .analysis import aggregator as agg
from .analysis import subhalo
from . import plot
from .dataset.imaging import SimulatorImaging
from .dataset.interferometer import SimulatorInterferometer
from .dataset.point_dataset import PointDataset
from .dataset.point_dataset import PointDict
from .fit.fit_imaging import FitImaging
from .fit.fit_interferometer import FitInterferometer
from .fit.fit_point import (
    FitPositionsSourceMaxSeparation,
    FitPositionsImage,
    FitPositionsSource,
    FitFluxes,
    FitPointDict,
    FitPointDataset,
)
from .lens.settings import SettingsLens
from .lens.ray_tracing import Tracer
from .lens.positions_solver import PositionsSolver
from .analysis.preloads import Preloads
from .analysis.analysis import AnalysisImaging, AnalysisInterferometer, AnalysisPoint
from .analysis.setup import SetupHyper

import slamutil

from confutil import conf

conf.instance.register(__file__)

__version__ = "2021.8.12.1"
