from arrayutil.preloads import Preloads
from arrayutil.dataset import preprocess
from arrayutil.dataset.imaging import SettingsImaging
from arrayutil.dataset.imaging import Imaging
from arrayutil.dataset.interferometer import Interferometer
from arrayutil.dataset.interferometer import SettingsInterferometer
from arrayutil.instruments import acs
from arrayutil.instruments import euclid
from arrayutil.inversion import pixelizations as pix
from arrayutil.inversion import regularization as reg
from arrayutil.inversion.inversion.settings import SettingsInversion
from arrayutil.inversion.inversion.imaging import (
    inversion_imaging_from as InversionImaging,
)
from arrayutil.inversion.inversion.interferometer import (
    inversion_interferometer_from as InversionInterferometer,
)
from arrayutil.inversion.mappers import mapper as Mapper
from arrayutil.inversion.pixelizations import SettingsPixelization
from arrayutil.mask.mask_1d import Mask1D
from arrayutil.mask.mask_2d import Mask2D
from arrayutil.mock import fixtures
from arrayutil.operators.convolver import Convolver
from arrayutil.operators.convolver import Convolver
from arrayutil.operators.transformer import TransformerDFT
from arrayutil.operators.transformer import TransformerNUFFT
from arrayutil.layout.layout import Layout2D
from arrayutil.structures.arrays.one_d.array_1d import Array1D
from arrayutil.structures.arrays.two_d.array_2d import Array2D
from arrayutil.structures.arrays.values import ValuesIrregular
from arrayutil.structures.arrays.abstract_array import Header
from arrayutil.structures.grids.one_d.grid_1d import Grid1D
from arrayutil.structures.grids.two_d.grid_2d import Grid2D
from arrayutil.structures.grids.two_d.grid_2d import Grid2DSparse
from arrayutil.structures.grids.two_d.grid_2d_interpolate import Grid2DInterpolate
from arrayutil.structures.grids.two_d.grid_2d_iterate import Grid2DIterate
from arrayutil.structures.grids.two_d.grid_2d_irregular import Grid2DIrregular
from arrayutil.structures.grids.two_d.grid_2d_irregular import Grid2DIrregularUniform
from arrayutil.structures.grids.two_d.grid_2d_pixelization import Grid2DRectangular
from arrayutil.structures.grids.two_d.grid_2d_pixelization import Grid2DVoronoi
from arrayutil.structures.vector_fields.vector_field_irregular import (
    VectorField2DIrregular,
)
from arrayutil.layout.region import Region1D
from arrayutil.layout.region import Region2D
from arrayutil.structures.kernel_2d import Kernel2D
from arrayutil.structures.visibilities import Visibilities
from arrayutil.structures.visibilities import VisibilitiesNoiseMap

from .analysis import aggregator as agg
from . import plot
from . import util
from .dataset.imaging import SimulatorImaging
from .dataset.interferometer import SimulatorInterferometer

from .fit.fit_imaging import FitImaging
from .fit.fit_interferometer import FitInterferometer
from .galaxy.fit_galaxy import FitGalaxy
from .galaxy.galaxy import Galaxy, HyperGalaxy, Redshift
from .galaxy.galaxy_data import GalaxyData
from .galaxy.masked_galaxy_data import MaskedGalaxyDataset
from .hyper import hyper_data
from .analysis.analysis import AnalysisImaging
from .analysis.analysis import AnalysisInterferometer
from galaxyutil.analysis.setup import SetupHyper
from .plane.plane import Plane
from .profiles import (
    point_sources as ps,
    light_profiles as lp,
    mass_profiles as mp,
    light_and_mass_profiles as lmp,
    scaling_relations as sr,
)
from . import convert

from confutil import conf

conf.instance.register(__file__)

__version__ = "2021.8.12.1"
